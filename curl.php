<?php 
class Curl {
    private $status;
    private $headerSent;

    public function __construct(CurlConnectData $req) 
    {
        $this->create($req);
    } 

    public function create($req) 
    {
        try {
            $ch = curl_init();
            $stat_copt = curl_setopt_array($ch, $req->getArr());
            if (! $stat_copt) {
                throw new Exception('Error for set curl options');
            }

            $this->webpage = curl_exec($ch); 
            if ($this->webpage === false) {
                throw new Exception(curl_error($ch), curl_errno($ch));
            }

            $this->status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $this->headerSent = curl_getinfo($ch, CURLINFO_HEADER_OUT);
            curl_close($ch); 
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function getHttpStatus()
    {
        return $this->status; 
    }

    public function getReqHeaders()
    {
        return $this->headerSent;
    }

    public function getResBody()
    { 
        return $this->webpage; 
    }
} 
