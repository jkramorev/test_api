<?php 

    require_once './curl.php';
    require_once './CurlConnectData.php';


    class TestApi 
    {
        private $token;
        private $url;

        public function __construct(
            string $token = 'token', 
            string $url   = 'https://demoapi.web-zaim.ru/v1/'
        ) {
            $this->token = $token;
            $this->url = $url;
        }

        public function getThesaurus()
        {

        }

        public function getTarif()
        {
            return $this->createCurlRequest($this->url . 'rates');
        }

        public function updateAccount($data)
        {
            return $this->createCurlRequest(
                $this->url . 'update', 
                $data,
                true
            );
        }

        public function addAccount($data)
        {
            return $this->createCurlRequest(
                $this->url . 'add', 
                $data,
                true
            );
        }

        /**
         * Create get or post request 
         * @param  string       $url     [description]
         * @param  array        $data    [description]
         * @param  bool|boolean $is_post [description]
         * @return [type]                [description]
         */
        private function createCurlRequest(
            string $url, 
            array $data = [],
            bool $is_post = false
        ) {
                $req = new CurlConnectData($url);
                $req->setCookieFileLocation('./cookie.txt')
                             ->setToken($this->token)
                             ->withHeaders()
                             ->setNoBody();
                if ($is_post) {
                    $req->setPost($data);
                }
                $res = new Curl($req);
                // var_dump($res->getHttpStatus());
                var_dump($res->getReqHeaders());
                // var_dump($res->getResBody());
                die();
                return (int) $res->getHttpStatus() === 200 
                       ? $res->getResBody()
                       : false;
        }
    }

    $accountdata = [
        'sum'           => 20300,
        'period'        => 10,
        'mob_phone'     => '89343453456',
        'email'         => 'strange@ya.ru',
        'last_name'     => 'Strangers',
        'first_name'    => 'Alexander',
        'middle_name'   => 'Venimatovich',
        'birthday'      => '1234-45-34',
        'gender'        => 'm',
        'snils'         => '0123456789',
        'inn'           => '270000000000',
        'birthplace'    => 'Владивосток',
        'authority'     => 21,
        'serial_number' => '123456',
        'issue_data'    => '2007-05-11',
        'issuer'        => "ОВД Кировского района г. Хабаровска",
        'subunit_code'  => '324-234',
        'registration_region'   => "2700000000000",
        'registration_area'     => "2700000000000",
        'registration_city'     => "2700000000000",
        'registration_street'   => "ul. Lenina",
        'registration_house'    => '123',
        'registration_building' => 'kA',
        'registration_flat'     => '72',
        'registration_index'    => '410000',
        'residential_region'    => "2700000000000",
        'residential_area'      => "2700000000000",
        'residential_city'      => "2700000000000",
        'residential_street'    => "ul. Lenina",
        'residential_house'     => '123',
        'residential_building'  => 'kA',
        'residential_flat'      => '72',
        'residential_index'     => '410000',
        'residential_phone'     => '123423451232',
        'education'             => '1',
        'marital_status'        => '1',
        'children_around'       => '2',
        'car'                   => '2',
        'income'                => '45532',
        'rent'                  => 8000,
        'relative_phone'        => "4212456789",
        "employer"              => "ООО «Пример»",
        "placement"             => "2017-01-01",
        "sector"                => "3",
        "position"              => "5",
        "employment_phone"      =>"4212445566",
        "work_region"           => "2700000000000",
        'work_area'             => "2700000000000",
        'work_city'             => "2700000100000",
        'work_street'           => "ул. Ленина",
        'work_house'            => '123',
        'work_building'         => "кА",
        'work_flat'             => "72",
        'work_index'            => '67800' 
    ];

    $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijg1ZDI5Nzk2MTY0MjM1ZjA2Zjc4MTM3YzA3YjIxODdjMDU0MWE1NzcyNWY4ZDAyNDZkZDIyMDk5NTk0YWMwMWE5MjQwMmE2OGUzOTYyNGM3In0.eyJhdWQiOiIyIiwianRpIjoiODVkMjk3OTYxNjQyMzVmMDZmNzgxMzdjMDdiMjE4N2MwNTQxYTU3NzI1ZjhkMDI0NmRkMjIwOTk1OTRhYzAxYTkyNDAyYTY4ZTM5NjI0YzciLCJpYXQiOjE1MDg3Mzc3MDgsIm5iZiI6MTUwODczNzcwOCwiZXhwIjoxNTQwMjczNzA4LCJzdWIiOiIxNiIsInNjb3BlcyI6W119.I4kZ8_m07Du3aHo9ZWmevA8V0IYHiiEkgBvyZTkpPtty6v_qHoXiCz8phibUxfTVYz52KgV0iwMTCZt_jqzMWoGODn-_J7Gc3udLTBWZpBz4kmdd--hES4iv9C4wGH99OMRiLHm31vRWD1t6o33v2KLQCZSqMIxJR3bCVR20TV_4lVNluPv7s5lNiGs5_-uLai1Q-mztb-iwZ43iA4nm3MuMLpr2D5utmMeB2deO3LvqJjICyGnlVHXjFsybkIlcB66E2dRk88rg_B_miMsfzdDjBfZZjz5Wd7aRCN2NrB8cxEZtW3UtrAr-Ygo9gB-VEpMx4i0Qn2lY5x9hGllsKPlUtS-0yVbS51Tm2wVmDo354y6xolyMKhf26uqoHwxt4OHaSK_2MLfZ49IMNhnmJKvUIAH_qcezrshmHU8hSmf7f46iWE07UEP7WLdf1cMCuRJ6cqWALEu8XZS-0CHedbzJtjrHgUHkEzSgvpm-97WgwPeGHVyNfYkkyCwmCeqfz55wwiRRHAA2ctrmzv_ZNeodXm5mgjZf6avVf42_2XNxU8pehVoIjH6OlR2I-RFLoRRt4VBSjHapqL3s3kjF_LIYwFfE-9TK63ZWvVp6ZI2XdUT1k_TeHuXbf5pBSAc_i-r5CD8I5zsTuu17_tqDi7vuNJsYckaLA6G4RYlqkaY}';
    $testApi = new TestApi($token);

    // $tarif = $testApi->getTarif();
    $addAccount = $testApi->addAccount($accountdata);
    // var_dump($tarif);
    var_dump($addAccount);
    // $accountdata['uid'] = '123';
    // $updateAccount = $testApi->addAccount($accountdata);
