<?php 
class CurlConnectData { 
    public  $useragent = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/61.0.3163.100 Chrome/61.0.3163.100 Safari/537.36'; 
    public  $url; 
    public  $followlocation; 
    public  $timeout = 30; 
    public  $maxRedirects = 4; 
    public  $cookieFileLocation = './cookie.txt'; 
    public  $post; 
    public  $postFields; 
    public  $referer ="http://www.lucifer.com"; 

    public  $session; 
    public  $webpage; 
    public  $includeHeader = true; 
    public  $noBody = false; 
    public  $status; 
    public  $binaryTransfer = false; 
    public  $authentication = 0; 
    public  $auth_name      = ''; 
    public  $auth_pass      = ''; 
    public  $curl_arr       = [];

    public function __construct(string $url)
    { 
        $this->url = $url; 
        $this->setCommonData();
    } 


    public function setCommonData()
    {
        $this->curl_arr = [
            CURLOPT_URL => $this->url,
            CURLOPT_CUSTOMREQUEST => 'GET', 
            CURLOPT_HTTPHEADER => [
                'Accept:application/json',
            ],
            CURLOPT_TIMEOUT     => $this->timeout, 
            CURLOPT_MAXREDIRS   => $this->maxRedirects, 
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => $this->followlocation,
            CURLOPT_COOKIEJAR   => $this->cookieFileLocation, 
            CURLOPT_COOKIEFILE  => $this->cookieFileLocation, 
            CURLINFO_HEADER_OUT => true,
            CURLOPT_USERAGENT => $this->useragent,
            CURLOPT_REFERER   => $this->referer
        ];
    }

    public function withAuth($name, $pass = '') 
    { 
       $this->curl_arr[CURLOPT_USERPWD] = $name. ':' . $pass; 
       return $this;
    } 

    public function setReferer($referer)
    {
        $this->curl_arr[CURLOPT_REFERER] = $referer;
        return $this;
    }

    public function setNoBody()
    {
        $this->curl_arr[CURLOPT_NOBODY] = true;
        return $this;
    }

    public function withHeaders()
    {
        $this->curl_arr[CURLOPT_HEADER] = true; 
        return $this;
    }


    public function setCookieFileLocation(string $path) 
    {
        $this->curl_arr[CURLOPT_COOKIEJAR] = $path; 
        $this->curl_arr[CURLOPT_COOKIEFILE] = $path; 
        return $this;
    }

    public function setPost(array $postFields) 
    {
        $this->curl_arr[CURLOPT_CUSTOMREQUEST] = 'POST';
        $this->curl_arr[CURLOPT_POST] = true;
        $this->curl_arr[CURLOPT_POSTFIELDS] = http_build_query($postFields, '', '&');
        return $this;
    }

    public function setUserAgent(string $userAgent) 
    {
        $this->curl_arr[CURLOPT_USERAGENT] = $userAgent;
        return $this;
    }

    public function setToken(string $token)
    {
        $this->curl_arr[CURLOPT_HTTPHEADER][1] = 'Authorization: Bearer ' . $token;
        return $this;
    }

    public function getArr(): array
    {
        return $this->curl_arr;
    }
} 
